# Visocular

Project responsible for building Docker images used by the [Minocular](https://gitlab.com/INSO-TUWien/Minocular) and [Labocular](https://gitlab.com/INSO-TUWien/Labocular) projects.

Note: account and project variables have to be lowercase!
